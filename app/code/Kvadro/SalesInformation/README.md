Synopsis
This extension add sales information  to magento products as extension attributes.

The example of adding extension attributes with plugin could be find in this extension

Parameter currentStatus added in Store->Configuration->Catalog->Product sales information-> General setting->Order Status 

The list of feeds can be accessed at the following url:

Extension Attributes can be accessible in product object by path: extension_attributes -> total_extension_information -> [ Qty(total in all orders with currentStatus),LastOrder(date last orderswith currentStatus)  could be find here]

Motivation
To demonstrate how to add extension attributes to product or to list of products

Technical features
API
In order to get product or list of products by Magento API you need to do API request to appropriate service. In Response you will see product object with described extension attributes You can find them by path, introduced below

Product Repository Plugin
You can find plugin here: Kvadro/SalesInformation/Model/Plugin/Product/ProductRepositoryInterfacePlugin afterGet, afterGetList - this methods are listen ProductRepositoryInterface in order to add there own attributes

Shipping information Loader
Shipping information links are loaded in plugin with ResourceModel/TotalSalesInformation help. You can get few external links by product id

Installation
This module is intended to be installed using composer. After the code is marshalled by composer, enable the module by adding it the list of enabled modules in the config or, if that file does not exist, installing Magento. After including this component and enabling it, you can verify it is installed by going the backend at:

STORES -> Configuration -> ADVANCED/Advanced -> Disable Modules Output

Once there check that the module name shows up in the list to confirm that it was installed correctly.

Database
In Database this module is represented by one table: product_sales_information and next fields: sale_id, order_id, order_status, product_id


Contributors
Igor Tson

License
Open Source License