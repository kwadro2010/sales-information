<?php

namespace Kvadro\SalesInformation\Api;

interface SalesInformationProviderInterface
{
    /**
     * @param int $productId
     * @return array
     */
    public function getTotalSalesInformation(int $productId):array;
}
