<?php

namespace Kvadro\SalesInformation\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface TotalSalesInformationInterface extends ExtensibleDataInterface
{

    /**
     *  Retrive qty product in  all orders
     *
     * @return int
     */
    public function getQty(): int;

    /**
     *  Retrive last orders date
     *
     * @return string
     */
    public function getLastOrder(): string;

    /**
     * @param \Kvadro\SalesInformation\Api\Data\TotalSalesInformationExtensionInterface $extensionAttributes
     * @return self
     */
    public function setExtensionAttributes(
        \Kvadro\SalesInformation\Api\Data\TotalSalesInformationExtensionInterface $extensionAttributes
    );

    /**
     * @return \Kvadro\SalesInformation\Api\Data\TotalSalesInformationExtensionInterface|null
     */
    public function getExtensionAttributes();
}
