<?php

namespace Kvadro\SalesInformation\Api\Data;

interface SalesInformationInterface
{
    const SALE_ID = "sale_id";
    const PRODUCT_ID = "product_id";
    const ORDER_ID = "order_id";
    const ORDER_STATUS = "order_status";

    /**
     * Set Sale Id for further updates
     *
     * @param int|null $saleId
     * @return self
     */
    public function setSaleId($saleId):self;

    /**
     * Retrieve sale id
     *
     * @return int|null
     */
    public function getSaleId();

    /**
     * Set Product Id for further updates
     *
     * @param int $productId
     * @return self
     */
    public function setProductId(int $productId):self;

    /**
     * Retrieve product id
     *
     * @return int|null
     */
    public function getProductId();

    /**
     * Set Order Id for further updates
     *
     * @param int $orderId
     * @return self
     */
    public function setOrderId(int $orderId):self;

    /**
     * Retrieve order id
     *
     * @return int|null
     */
    public function getOrderId();

    /**
     * Set Order Status for further updates
     *
     * @param string $orderStatus
     * @return self
     */
    public function setOrderStatus(string $orderStatus):self;

    /**
     * Retrieve order status
     *
     * @return string|null
     */
    public function getOrderStatus();
}
