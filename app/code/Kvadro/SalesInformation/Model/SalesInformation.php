<?php

namespace Kvadro\SalesInformation\Model;

use Kvadro\SalesInformation\Api\Data\SalesInformationInterface;

/**
 * Load and save data  with  additional order information
 *
 * Class SalesInformation
 */
class SalesInformation implements SalesInformationInterface
{
    /** @var  int */
    private $saleId;
    /** @ar  int */
    private $orderId;
    /** @var  int */
    private $productId;
    /** @var  int */
    private $orderStatus;

    /**
     * Retrieve sale id
     *
     * @return int|null
     */
    public function getSaleId()
    {
        return $this->saleId;
    }

    /**
     * Set Sale Id for further updates
     *
     * @param int|null $saleId
     * @return self
     */
    public function setSaleId($saleId): SalesInformationInterface
    {
        $this->saleId = $saleId;
        return $this;
    }

    /**
     * Retrieve order status
     *
     * @return string|null
     */
    public function getOrderStatus()
    {
        return $this->orderStatus;
    }

    /**
     * Set Order Status for further updates
     *
     * @param string $orderStatus
     * @return self
     */
    public function setOrderStatus(string $orderStatus): SalesInformationInterface
    {
        $this->orderStatus = $orderStatus;
        return $this;
    }

    /**
     * Retrieve order id
     *
     * @return int|null
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set Order Id for further updates
     *
     * @param int $orderId
     * @return self
     */
    public function setOrderId(int $orderId): SalesInformationInterface
    {
        $this->orderId = $orderId;
        return $this;
    }

    /**
     * Retrieve product id
     *
     * @return int|null
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set Order Id for further updates
     *
     * @param int $productId
     * @return self
     */
    public function setProductId(int $productId): SalesInformationInterface
    {
        $this->productId = $productId;
        return $this;
    }
}
