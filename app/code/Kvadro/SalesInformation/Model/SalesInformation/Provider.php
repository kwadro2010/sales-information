<?php

namespace Kvadro\SalesInformation\Model\SalesInformation;

use Kvadro\SalesInformation\Api\SalesInformationProviderInterface;
use Kvadro\SalesInformation\Model\ResourceModel\SalesInformation;
use Kvadro\SalesInformation\Model\SalesInformationFactory;
use Magento\Framework\EntityManager\EntityManager;

/**
 * Load  additional sales information for product
 *
 * Class Provider
 */
class Provider implements SalesInformationProviderInterface
{
    /** @var  EntityManager */
    private $entityManager;

    /** @var SalesInformationResource  */
    private $salesInformationResource;

    /** @var  SalesInformationFactory */
    private $salesInformationFactory;

    /**
     * Provider constructor.
     * @param EntityManager $entityManager
     * @param SalesInformation $salesInformationResource
     * @param SalesInformationFactory $salesInformationFactory
     */
    public function __construct(
        EntityManager $entityManager,
        SalesInformation $salesInformationResource,
        SalesInformationFactory $salesInformationFactory
    ) {
        $this->entityManager = $entityManager;
        $this->salesInformationResource = $salesInformationResource;
        $this->salesInformationFactory = $salesInformationFactory;
    }

    /**
     * @param int $productId
     * @return array
     * @throws \Exception
     */
    public function getTotalSalesInformation(int $productId):array
    {
        /** @var array $salesInformatics */
        $salesInformatics = [];
        $ids = $this->salesInformationResource->getSaleIdsByProductId($productId);
        foreach ($ids as $id) {
            $salesInformation = $this->salesInformationFactory->create();
            $salesInformatics[] = $this->entityManager->load($salesInformation, $id);
        }
        return $salesInformatics;
    }
}
