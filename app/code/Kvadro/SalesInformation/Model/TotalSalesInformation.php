<?php

namespace Kvadro\SalesInformation\Model;

use Kvadro\SalesInformation\Api\Data\TotalSalesInformationInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Load and save data  for  extension attribute total_sales_information_
 *
 * Class TotalSalesInformation
 */
class TotalSalesInformation extends AbstractModel implements IdentityInterface, TotalSalesInformationInterface
{
    /**
     * Tag  for cache identification
     */
    const CACHE_TAG = 'kvadro_salesinformation_salesinformation_total';
    /** @var  int */
    private $productId;
    /** @var  int */
    private $orderStatus;
    /** @var  array */
    private $extensionAttributes;

    /**
     * @param array $data
     * @return void
     */
    protected function _construct(array $data = [])
    {
        $this->_init(\Kvadro\SalesInformation\Model\ResourceModel\TotalSalesInformation::class);
    }

    public function getProductId()
    {
        return $this->productId;
    }

    public function setProductId(int $id): TotalSalesInformationInterface
    {
        $this->productId = $id;
        return $this;
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return string[]
     */
    public function getIdentities(): array
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     *  Retrive last orders date with $orderStatus  for product with $productId
     *
     * @return string
     */
    public function getLastOrder(): string
    {
        $productId = $this->getProductId();
        return $this->getResource()->getLastOrderDate($productId);
    }

    /**
     *  Retrive qty product in all orders with $orderStatus
     *
     * @return int
     */
    public function getQty(): int
    {
        $productId = $this->getProductId();
        return $this->getResource()->getQty($productId);
    }
    /**
     * @param \Kvadro\SalesInformation\Api\Data\TotalSalesInformationExtensionInterface $extensionAttributes
     * @return TotalSalesInformationInterface
     */
    public function setExtensionAttributes(
        \Kvadro\SalesInformation\Api\Data\TotalSalesInformationExtensionInterface $extensionAttributes
    ) {
        $this->extensionAttributes = $extensionAttributes;
        return $this;
    }
    /**
     * @return \Kvadro\SalesInformation\Api\Data\TotalSalesInformationExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->extensionAttributes;
    }
}
