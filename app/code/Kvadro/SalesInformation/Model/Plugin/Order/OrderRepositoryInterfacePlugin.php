<?php

namespace Kvadro\SalesInformation\Model\Plugin\Order;

use Kvadro\SalesInformation\Api\SalesInformationProviderInterface;
use Kvadro\SalesInformation\Model\SalesInformation;
use Kvadro\SalesInformation\Model\SalesInformationFactory;
use Kvadro\SalesInformation\Model\SalesCacheService;
use Magento\Framework\EntityManager\EntityManager;
use Magento\Framework\Exception\CouldNotSaveException;

/**
 * Class OrderRepositoryInterfacePlugin
 */
class OrderRepositoryInterfacePlugin
{
    /** @var SalesInformationFacrory */
    private $salesInformationProvider;

    /** @var SalesInformationFactory */
    private $salesInformationFactory;

    /** @var SalesCacheService  */
    private $salesCacheService;

    /** @var EntityManager */
    private $entityManager;

    public function __construct(
        SalesInformationProviderInterface $salesInformationProvider,
        SalesInformationFactory $salesInformationFactory,
        SalesCacheService $salesCacheService,
        EntityManager $entityManager
    ) {
        $this->salesInformationProvider = $salesInformationProvider;
        $this->salesInformationFactory = $salesInformationFactory;
        $this->salesCacheService = $salesCacheService;
        $this->entityManager = $entityManager;
    }

    /**
     * Retrive sales information for all products in order
     *
     * @param \Magento\Sales\Api\OrderRepositoryInterface $subject
     * @param \Magento\Sales\Api\Data\OrderInterface $resultOrder
     * @return \Magento\Sales\Api\Data\OrderInterface
     * @throws CouldNotSaveException
     */
    public function afterSave(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        \Magento\Sales\Api\Data\OrderInterface $resultOrder
    ) {
        $resultOrder = $this->saveSalesInformation($resultOrder);

        return $resultOrder;
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return \Magento\Sales\Api\Data\OrderInterface
     * @throws CouldNotSaveException
     */
    private function saveSalesInformation(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        $orderItems = $order->getItems();
        $orderId = $order->getId();
        $orderStatus =$order->getStatus();
        $salesInformationItems = [];

        foreach ($orderItems as $orderItem) {
            $productId = $orderItem->getProductId();
            $salesInformationItems [$productId] = $this->salesInformationFactory->create()
                ->setSaleId(null)
                ->setOrderStatus($orderStatus)
                ->setProductId($orderItem->getProductId())
                ->setOrderId($orderId);

            foreach ($salesInformationItems as $productId => $salesInformationItem) {
                try {
                    $oldSalesInformationItems = $this->salesInformationProvider->getTotalSalesInformation($productId);
                    if ($oldSalesInformationItems) {
                        $this->deleteOldSalesInformation($salesInformationItem, $oldSalesInformationItems);
                    }
                    $this->entityManager->save($salesInformationItem);

                    /** clean all data  in cache for product with $productId  */
                    $this->salesCacheService->cleanAllDataCacheByProductId($productId);
                } catch (\Exception $e) {
                    throw new CouldNotSaveException(
                        __('Could not add sales info to sales information : "%1"', $e->getMessage()),
                        $e
                    );
                }
            }
        }

        return $order;
    }

    /**
     *  Delete old sale information  with identically order_id
     *
     * @param SalesInformation $salesInformationItems
     * @param array $oldSalesInformationItems
     * @throws \Exception
     */
    private function deleteOldSalesInformation(SalesInformation $salesInformationItems, array $oldSalesInformationItems)
    {
        foreach ($oldSalesInformationItems as $oldSalesInformationItem) {
            if ($oldSalesInformationItem->getOrderId() == $salesInformationItems->getOrderId()) {
                $this->entityManager->delete($oldSalesInformationItem);
            }
        }
    }
}
