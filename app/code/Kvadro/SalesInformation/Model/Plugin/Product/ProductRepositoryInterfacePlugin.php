<?php

namespace Kvadro\SalesInformation\Model\Plugin\Product;

use Kvadro\SalesInformation\Api\SalesInformationProviderInterface;
use Kvadro\SalesInformation\Model\TotalSalesInformationFactory;
use Magento\Catalog\Api\Data\ProductExtensionFactory;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchResults;
use Magento\Framework\EntityManager\EntityManager;

/**
 * Class class ProductRepositoryInterfacePlugin
 */
class ProductRepositoryInterfacePlugin
{
    /** @var ProductExtensionFactory */
    private $productExtensionFactory;

    /** @var SalesInformationProviderInterface */
    private $salesInformationProvider;

    /** @var TotalSalesInformationFactory */
    private $TotalSalesInformationFactory;

    /**
     * Repository constructor.
     * @param ProductExtensionFactory $productExtensionFactory
     * @param EntityManager $entityManager
     * @param SalesInformationProviderInterface $salesInformationProvider
     */
    public function __construct(
        ProductExtensionFactory $productExtensionFactory,
        SalesInformationProviderInterface $salesInformationProvider,
        TotalSalesInformationFactory $TotalSalesInformationFactory
    ) {
        $this->productExtensionFactory = $productExtensionFactory;
        $this->salesInformationProvider = $salesInformationProvider;
        $this->TotalSalesInformationFactory = $TotalSalesInformationFactory;
    }

    /**
     * Add Sales Information to product extension attributes
     *
     * @param ProductRepositoryInterface $subject
     * @param SearchResults $searchResult
     * @return SearchResults
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetList(
        ProductRepositoryInterface $subject,
        SearchResults $searchResult
    ) {
        /** @var ProductInterface $product */
        foreach ($searchResult->getItems() as $product) {
            $this->addTotalSalesInformationToProduct($product);
        }

        return $searchResult;
    }

    /**
     * @param ProductRepositoryInterface $subject
     * @param ProductInterface $product
     * @return ProductInterface
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGet(
        ProductRepositoryInterface $subject,
        ProductInterface $product
    ) {
        $this->addTotalSalesInformationToProduct($product);

        return $product;
    }

    /**
     * @param ProductInterface $product
     * @return self
     */
    private function addTotalSalesInformationToProduct(ProductInterface $product)
    {
        $extensionAttributes = $product->getExtensionAttributes();
        if (empty($extensionAttributes)) {
            $extensionAttributes = $this->productExtensionFactory->create();
        }
        $TotalSalesInformation = $this->TotalSalesInformationFactory->create();
        $TotalSalesInformation->setProductId($product->getId());
        $extensionAttributes->setTotalSalesInformation($TotalSalesInformation);
        $product->setExtensionAttributes($extensionAttributes);

        return $this;
    }
}
