<?php

namespace Kvadro\SalesInformation\Model\ResourceModel;

use Kvadro\SalesInformation\Api\Data\SalesInformationInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\EntityManager\MetadataPool;

/**
 * Load data for extension attribute total_sales_information
 * return  qty  and last order date for product
 *
 * Class SalesInformation
 */
class SalesInformation
{
    /** @var  MetadataPool */
    private $metadataPool;
    /** @var  ResourceConnection\ */
    private $resourceConnection;

    /**
     * SalesInformation constructor.
     * @param MetadataPool $metadataPool
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        MetadataPool $metadataPool,
        ResourceConnection $resourceConnection
    ) {
        $this->metadataPool = $metadataPool;
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param $productId
     * @return array
     * @throws \Exception
     */
    public function getSaleIdsByProductId(int $productId):array
    {
        $metadata = $this->metadataPool->getMetadata(SalesInformationInterface::class);
        $connection = $this->resourceConnection->getConnection();
        $select = $connection
            ->select()
            ->from($metadata->getEntityTable(), SalesInformationInterface::SALE_ID)
            ->where(SalesInformationInterface::PRODUCT_ID . ' = ?', $productId);
        $ids = $connection->fetchCol($select);
        return $ids ?: [];
    }
}
