<?php

namespace Kvadro\SalesInformation\Model\ResourceModel;

use Kvadro\SalesInformation\Api\Data\SalesInformationInterface;
use Kvadro\SalesInformation\Model\SalesCacheService;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Store\Model\ScopeInterface;

/**
 *  Load data for extension attribute total_sales_information
 *
 * Class TotalSalesInformation
 */
class TotalSalesInformation extends AbstractDb
{
    /**
     * Tables name for sales_information
     */
    const SALES_INFORMATION_TABLE = 'product_sales_information';

    /**
     *  Path in xml setting  to current status order
     */
    const SHIPPING_INFO__ORDER_STATUS = 'kv_sales_information/general/order_status_sales_information';

    /** @var MetadataPool */
    private $metadataPool;

    /** @var OrderCollectionFactory */
    private $orderCollectionFactory;

    /** @var SalesCacheService */
    private $salesCacheService;

    /** @var ScopeConfigInterface */
    private $scopeConfig;

    public function __construct(
        MetadataPool $metadataPool,
        OrderCollectionFactory $orderCollectionFactory,
        SalesCacheService $salesCacheService,
        ScopeConfigInterface $scopeConfigInterface,
        Context $context,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->metadataPool = $metadataPool;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->salesCacheService = $salesCacheService;
        $this->scopeConfig = $scopeConfigInterface;
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            self::SALES_INFORMATION_TABLE,
            SalesInformationInterface::SALE_ID
        );
    }

    /**
     * @param int $productId
     * @param string $orderStatus
     * @return int
     * @throws \Exception
     */
    public function getQty(int $productId): int
    {
        $productQtyTotal = $this->salesCacheService->loadQtyTotalFromCache($productId);
        if (!$productQtyTotal) {
            $productQtyTotal = 0;
            $orderIds = $this->getOrderIdsByProductId($productId);
            if (!empty($orderIds)) {
                return 0;
            }
            $orders = $this->orderCollectionFactory->create()->addFieldToFilter('entity_id', ['in' => $orderIds]);
            if ($orders && count($orders) > 0) {
                foreach ($orders as $order) {
                    $orderItems = $order->getAllVisibleItems();
                    foreach ($orderItems as $orderItem) {
                        if ((int)$orderItem->getProductId() == $productId) {
                            $productQtyTotal += (int)$orderItem->getQtyOrdered();
                            break;
                        }
                    }
                }
            }
            $this->salesCacheService->saveQtyTotalFromCache($productQtyTotal, $productId);
        }

        return $productQtyTotal;
    }

    /**
     * @param int $productId
     * @return string
     * @throws \Exception
     */
    public function getLastOrderDate(int $productId): string
    {
        $lastOrderDate = $this->salesCacheService->loadLastOrderFromCache($productId);
        if (!$lastOrderDate) {
            $lastOrderDate = '';
            $orderIds = $this->getOrderIdsByProductId($productId);
            if (!empty($orderIds)) {
                $orders = $this->orderCollectionFactory->create()
                    ->addAttributeToSelect('updated_at')
                    ->addFieldToFilter('entity_id', ['in' => $orderIds])
                    ->addAttributeToSort('updated_at', 'DESC')
                    ->setOrder('updated_at', "DESC");
                if ($orders && count($orders) > 0) {
                    $lastOrderDate = $orders->getFirstItem()->getUpdatedAt();
                }
            }

            $this->salesCacheService->saveLastOrderFromCache($lastOrderDate, $productId);
        }
        return $lastOrderDate;
    }

    /**
     * @param $productId
     * @return array
     * @throws \Exception
     */
    private function getOrderIdsByProductId($productId)
    {
        $orderStatus = $this->getCurrentOrderStatus();
        $metadata = $this->metadataPool->getMetadata(SalesInformationInterface::class);
        $connection = $this->getConnection();

        $select = $connection
            ->select()
            ->from($metadata->getEntityTable(), SalesInformationInterface::ORDER_ID)
            ->where(SalesInformationInterface::PRODUCT_ID . ' = ?', $productId)
            ->where(SalesInformationInterface::ORDER_STATUS . ' = ?', $orderStatus);
        $ids = $connection->fetchCol($select);
        return $ids ?: [];
    }

    /**
     * @return string
     */
    private function getCurrentOrderStatus()
    {

        return $this->scopeConfig->getValue(self::SHIPPING_INFO__ORDER_STATUS, ScopeInterface::SCOPE_STORE);
    }
}
