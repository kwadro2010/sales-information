<?php

namespace Kvadro\SalesInformation\Model;

use Magento\Framework\App\Cache\Manager as CacheManager;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\Serialize\SerializerInterface;

/**
 *  Load  save and clean  data total_sales_information attribute in cache
 *
 * Class SalesCacheService
 */
class SalesCacheService
{
    /**
     * @var string   Prefix for identification in cache   data last order
     */
    const CACHE_TAG_LAST_ORDER = "sales_information_last_order_";

    /**
     * @var string   Prefix for identification in cache  total sales
     */
    const CACHE_TAG_SALES_QTY_TOTAL = "sales_information_qty_total_";

    /** @var SerializerInterface */
    private $serializer;

    /** @var CacheInterface */
    private $cacheInterface;
    private $cacheManager;

    public function __construct(
        SerializerInterface $serializer,
        CacheInterface $cacheInterface,
        CacheManager $cacheManager
    ) {
        $this->serializer = $serializer;
        $this->cacheInterface = $cacheInterface;
        $this->cacheManager = $cacheManager;
    }

    /**
     *  Load   lastOrder with cache by product with $productId
     *
     * @param $productId
     * @return string|null
     */
    public function loadLastOrderFromCache($productId)
    {
        $cacheIdentify = self::CACHE_TAG_LAST_ORDER . $productId;
        return $this->loadDataFromCache($cacheIdentify);
    }

    /**
     *  Load   qtyTotal with cache by product with $productId
     *
     * @param $productId
     * @return string|null
     */
    public function loadQtyTotalFromCache($productId)
    {
        $cacheIdentify = self::CACHE_TAG_SALES_QTY_TOTAL . $productId;
        return $this->loadDataFromCache($cacheIdentify);
    }

    /**
     *  Save $lastOrder in cache  by product with $productId
     *
     * @param $lastOrder
     * @param $productId
     */
    public function saveLastOrderFromCache($lastOrder, $productId)
    {
        $cacheIdentify = self::CACHE_TAG_LAST_ORDER . $productId;
        return $this->saveDataInsideCache($lastOrder, $cacheIdentify);
    }

    /**
     *  Save $qtyTotal in cache  by product with $productId
     *
     * @param $qtyTotal
     * @param $productId
     */
    public function saveQtyTotalFromCache($qtyTotal, $productId)
    {
        $cacheIdentify = self::CACHE_TAG_SALES_QTY_TOTAL . $productId;
        return $this->saveDataInsideCache($qtyTotal, $cacheIdentify);
    }

    /**
     * Load data  by $cacheIdentify
     *
     * @param $cacheIdentify
     * @return string
     */
    private function loadDataFromCache($cacheIdentify)
    {
        return $this->cacheInterface->load($cacheIdentify);
    }

    /**
     * Save  $cacheValue with  identify $cacheIdentify
     *
     * @param $cacheValue
     * @param $cacheIdentify
     */
    private function saveDataInsideCache($cacheValue, $cacheIdentify)
    {
        $cacheValue = $this->serializer->serialize($cacheValue);
        $this->cacheInterface->save($cacheValue, $cacheIdentify);
    }

    /**
     * @param array $cacheIdentifies
     */
    private function removeDataCache(array $cacheIdentifies)
    {
        foreach ($cacheIdentifies as $cacheIdentify) {
            $this->cacheInterface->remove($cacheIdentify);
        }
    }

    /**
     * remove all cache data for product with $productId
     *
     * @param $productId
     */
    public function cleanAllDataCacheByProductId($productId)
    {
        $cacheIdentifies = [];
        $cacheIdentifies[] = self::CACHE_TAG_LAST_ORDER . $productId;
        $cacheIdentifies[] = self::CACHE_TAG_SALES_QTY_TOTAL . $productId;
        $this->removeDataCache($cacheIdentifies);
    }
}
